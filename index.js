"use strict"
/* Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?
Це метод об'єкту Event який зупиняє стандартну подію браузера.

2. В чому сенс прийому делегування подій?
Сенс полягає  в тому що ми не прописуємо 1000, а може і більше add.eventListener, 
а прописуємо його один раз батьківському контейнеру. 

3. Які ви знаєте основні події документу та вікна браузера? 
DOMContentLoaded;
Події миші:click, contextmenu, mouseover/mouseout,
mousedown/mouseuo;
Події клавіатури: keydown/keyup;
Події елементів форми: submit,focus.

Практичне завдання:

Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:

- У папці tabs лежить розмітка для вкладок. 
Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки.
 При цьому решта тексту повинна бути прихована. 
 У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та 
видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не
 переставала працювати.

 Умови:
 - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
*/

const tabs = document.querySelector(".tabs");
const tabsContentCollection = document.querySelectorAll(".tabs-content li");

const tabsCollection = document.querySelectorAll(".tabs-title");


tabs.addEventListener("click", (event) => {

    tabsCollection.forEach(el => {
        if (el.className.includes("active")) {
            el.classList.remove("active");
        }
    })

    event.target.classList.add("active");

    tabsContentCollection.forEach(elem => {
        elem.classList.add("hidden");
        const element = document.querySelector(`.${event.target.innerText.toLowerCase()}-content`);

        element.classList.remove("hidden");

    })

})

